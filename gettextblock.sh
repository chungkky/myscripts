#! /bin/sh
# script name: gettextblock.sh

# print content enclosed by <text></text> tags inclusive
# in sourcefile $1
#
# To run script, enter: 
# $ gettextblock.sh sourcefilename outputfilename
#
sed -n "/\<text/,/\<\/text/p" $1 > $2