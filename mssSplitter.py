#!/usr/bin/env python
#
# source.txt is name of your source file
#
# check that you enter the name and extension you wish for the output file
#
# check that you have the right record separator string
#
# run file at command line by typing: $ python mssSplitter.py
#
with open('records.xml') as source:  # open 'source.txt' file and assign alias of "source" to it
with open('mssLookupString.txt') as mssList # open 'mssLookupString.txt' file and assign alias of "mssList" to it
	output=''
	start=0
	count=1
# 
	for line in source.read().split("\n"):  # reads line into an array line
		for mssString in mssList.read().split("\n"): # reads mssString into another array line
			if (line=='***'):  # this is the record separator string
				if (start==1):
					# put desired output file name and extension in next line
					with open('MTPB_' + str(count) + '.xml', 'w') as outfile:
						outfile.write(output)
						outfile.close()
						output=''
						count+=1
				else:
					start=1
			else:
				if (output==''):
					output = line
				else:
					output = output + '\n' + line
	source.close()