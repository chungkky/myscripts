#!/bin/bash
#
# Shell script for renaming multiple (.xml) files
#
# Source file is in the "*.xml" location, first (for loop) line of the script.
# Script assumes you are in the directory where your source files are stored;
# if this is not the case, include the path to the location of the files.
#
# "OldPattern" in the file name is replaced by "NewPattern"
# in the file name
#
for filename in *.xml; do
	mv $filename $(echo $filename | sed 's/OldPattern/NewPattern/g')
done