#!/bin/bash

# filename:  catMSS.sh
# 
# To run the file: 
#  If you want output in same directory: $ catMSS.sh <sourcefile>
#  If you want output in different directory: $ catMSS.sh <sourcefile> <destinationDirectory>

while IFS='' read -r line || [[ -n "$line" ]]; do

#   echo "Text read from file: $line"
#	cat ${line}* > ${line}.txt   # if you want output in same directory
	cat ${line}* > "$2"${line}.txt   # output in different directory "$2"
done < "$1"


# Adapted from <https://stackoverflow.com/questions/10929453/read-a-file-line-by-line-assigning-the-value-to-a-variable/10929511#10929511>
# Explanation:
#
#    IFS='' (or IFS=) prevents leading/trailing whitespace from being trimmed.
#    -r prevents backslash escapes from being interpreted.
#    || [[ -n $line ]] prevents the last line from being ignored if it doesn't 
#  end with a \n (since read returns a non-zero exit code when it encounters EOF).
#
#
# As one line, non-interactive:
# $ while IFS='' read -r line || [[ -n "$line" ]]; do cat "$line"* > ${line}-all.txt; done < sourcefilename 