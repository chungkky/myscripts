#!/usr/bin/env python
#
# Script to rename files according to input from a list of names
# 
# info from <https://www.geeksforgeeks.org/rename-multiple-files-using-python/>
#
# import the OS module in Python; Rename is in the OS module
# Syntax for os.rename(): is os.rename(src, dest): where src is source/old
# filename and dest is destination/new filename
#
# import OS module
import os

# Function to rename multiple files in directory xyz to something like "Hostel1", "Hostel2", etc.
# The rename() function renames files or directories
# The listdir() function lists the content of a directory. Syntax is
#    list = os.listdir('Src') where Src is the source to be listed
def main()
	i = 0
	
	
	for filename in os.listdir("xyz"):
		dst = "Hostel" + str(i) + ".xml"
		src = 'xyz'+ filename
		dst = 'xyz' + dist
		
		# rename function will
		# rename all the files
		os.rename(src, dst)
		i += 1
		
# Driver Code
if __name__ == '__main__':

	# Calling main() function
	main()
