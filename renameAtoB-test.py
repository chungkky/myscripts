#!/usr/bin/env python

# File name: renameAtoB-test.py
# 
# to run this file, enter: $ python renameAtoB-test.py oldfilename newfilename
#
# import sys libary which allow you to use arguments with python CLI
import sys
#print('Number of arguments is', len(sys.argv), 'arguments')
print (sys.argv[1], sys.argv[2])

# Read file "a.txt" into a list, striping away the \n
# at the end of the lines
#
with open(sys.argv[1]) as f:
    RecName = [line.rstrip() for line in f]
#    print(RecName)
#
# Read file "b.txt" into a list, striping away the \n
# at the end of the lines

with open(sys.argv[2]) as g:
    NewRecName = [line.rstrip() for line in g]
#    print(NewRecName)

# import the os library which has the rename funtion
import os
#
for i in range(len(RecName)):  # the "i" is used for indexing the lists
#	print(RecName[i], NewRecName[i])  # this is to test that the strings are correct
	os.rename(RecName[i], NewRecName[i])
#
# End
