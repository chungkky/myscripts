#!/bin/bash
#
# Script Description:
# 
#
# run script:  $ replaceStringAFileAwithStringBFileB.sh
#
while read -d '\t' stringA stringBfilename; do
  sed -e 's/${stringA}/${stringB}/g' filename.txt
done < lookup.txt
#
# <filename.txt> is the file where you are doing the search and replace
# <lookup.txt> is the TSV (tab separated value) file containing search stringA and replacement stringB
