#! /bin/sh
#
# First you have to convert the MSWord file into a plain text file.
# 1. use "Save As" to save the MSWord file as a TXT file with UTF-8 and LF/CR
#
# 2. Rename the new file "salisEN.txt"
#
# 3. Remove any non-record text from beginning and end of the file.
#
# ==================================================
# script name: doSalis.sh
#
# To run script, either enter: $ doSalisEN.sh
#                          OR  $ doSalisEN.sh sourcefile.txt
#
#  (if you use the latter, sourcefile.txt is salis.txt and will
#  be assign variable name $1, so comment out/in the version 
#  below which matches your choice.)
#
# you can put set -x and set -e at the top of your scripts. 
# The -x tells the shell to print out every command before it executes it. 
# The -e tells the shell to terminate the script if any errors occur.
#
set -x
set -e
#
# -------------------------------------------------
# Count how many records there are by counting "@h" lines.
# The "@h" lines have the pattern of three bits of info delimited by a "!":
# @h\SAR!date!mssCode\!
#
# Use grep to extract lines containing "@h" string, pipe it to a count lines
# and return the info to the screen.
#
grep "@h" salisEN.txt | wc -l 
# grep "@h" $1 | wc -l 
#
# -------------------------------------------------
# Print out a file containing all the "@h" lines.
#
grep "@h" salisEN.txt > enAtHLines.txt
# grep "@h" $1 > salisAtHLines.txt
#
# grep -n "@h" salis.txt > salisAtHLines+LineNum.txt
#
# -------------------------------------------------
# Remove all the "\" from the "@h" lines making it so that the delimiter is just the "!".
#
# sed 's/\\//g' salisAtHlines.txt > salisAtHlinesNoSlash.txt
#
# -------------------------------------------------
# Print out a file containing the mss codes in the same order as the records
#
awk -F "\!" '{ print $2 }' enAtHLines.txt > enAllMssCodes.txt
#
# --------------------------------------------------
# Sort the file enAtHlines.txt using the second "field" as the key.
# Do not use numberic sort "-n".
# "-k" is by key.
# "2,2" means the key begins with 2nd field and ends with 2nd field.
# "-t \!" means the delimiter is "!", you have to escape the "!".
#
sort -t "\!" -k 2,2 enAtHLines.txt > enMssCodesSort.txt
#
# -------------------------------------------------
# Output a file containing the unique mss codes, sorted 
# alphbetically.
# The -F "\!" tells awk that the delimiter is "!".
# The $2 indicates the second field in a line (the mss code).
#
awk -F "\!" '{ print $2 }' enMssCodesSort.txt | uniq > enMssCodesUnique.txt
#
# -------------------------------------------------
# Output a file containing the mss codes, sorted alphbetically, 
# with a count of how many times that code appears.
# The -F "\!" tells awk that the delimiter is "!".
# The $2 indicates the second field in a line (the mss code).
#
awk -F "\!" '{ print $2 }' enMssCodesSort.txt | uniq -c > enMssCodesSummary.txt
#
# -------------------------------------------------
# DRAFT:
# Split the single large file "salis.txt" into separate
# files which contain one record each using a modified
# version of a one line shell script I wrote earlier,
# Script Name: splittextblocks.sh
#
# Script Purpose: Originally for REEDLondon XML files. To split <text></text> blocks 
# in a large file containing multiple records for use in Oxygen into 
# multiple files for upload to CWRC Writer.
#
# Note: output file name pattern is in the quotations 
# of the x="filename"++i".xml" part of the line.
# The new files will be named "filename1.xml", "filename2.xml", etc.
#
# awk '/*****/{x="record"++i".xml";}{print > x;}' salis.txt
#
# -------------------------------------------------------------
# Split file on delmiiter line "@h"
# And move output files to another directory for adding extension
#
# Split a large file based on a delimiter line. Resulting files
# have no file extension. So you need to rename resulting files 
# using above script from Script Ahoy to add the file extension.
#
# Current script begins number indexing at 000, quick and dirty
# way for it begin number indexing files at 001 is to add an
# extra dummy "record" at the beginning of salis.txt and let
# that record be #000, then the first real record will be
# the second "@h" line but numbered #001.
#
# From: # <http://www.lostsaloon.com/technology/how-to-split-a-file-into-multiple-files-in-linux/>
# and manual page for "csplit"

 csplit -k -n 3 -f enRec salisEN.txt /^@h/ {500}

# The "-k" means do not remove output files if an error occurs or 
# a HUP, INT or TERM signal is received.
# The "-n 3" says to use 3-digits for the file output.
# The "-f enRec" makes the prefix of resulting files "enRec".
# The "/^@h/" is the delimiter string which marks start of a record.
# The {500} sets upper limit for how many times this command
# is executed. Set the number to be greater than number of records.
# Together this means resulting files have name pattern:
# "enRec000", "enRec001", "enRec002", etc.
#
# -------------------------------------------
# Move all resulting salisRec files (numbered 000-N) to a different directory
#

mkdir enBits/
cp enRec* enBits/

# ---------------------------------
# Move the output files into a separate folder, 
# then add ".txt" file extension to all the 
# resulting output files.
#
# Script purpose: Append extension to file name (From Script Ahoy)
# Remove comment to run the shell script below.

for files in enRec*; do mv ${files} ${files}.txt; done


