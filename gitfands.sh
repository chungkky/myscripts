#!/bin/bash
#
# Script name: gitfands.sh ('git fetch and status')
# A script to run 'git fetch origin' followed by
# 'git status'.
#
git fetch origin
git status
#
# -- end --