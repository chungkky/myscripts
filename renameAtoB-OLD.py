#!/usr/bin/env python

# File name: renameAtoB.py
# 
# to run this file, enter: $ python renameAtoB.py

# Read file "a.txt" into a list, striping away the \n
# at the end of the lines
#
with open('a.txt') as f:
    RecName = [line.rstrip() for line in f]
#    print(RecName)
#
# Read file "b.txt" into a list, striping away the \n
# at the end of the lines
#
with open('b.txt') as g:
    NewRecName = [line.rstrip() for line in g]
#    print(NewRecName)

# import the os library which has the rename funtion
import os
#
for i in range(len(RecName)):  # the "i" is used for indexing the lists
#	print(RecName[i], NewRecName[i])  # this is to test that the strings are correct
	os.rename(RecName[i], NewRecName[i])
#
# End
