#! /bin/sh
# Script Name: splittextblocks.sh
#
# Script Purpose: Originally for REEDLondon XML files. To split <text></text> blocks 
# in a large file containing multiple records for use in Oxygen into 
# multiple files for upload to CWRC Writer.
#
# Usage: 
# 1. might have to remove TEI header from source file first
# 2. enter name of output file name string below, then 
# 3. type "splittextblocks.sh sourcefilename" into the command line
#
# Note: output file names are in the quotations 
# of the x="output"++i".xml" part of the line
#
awk '/\<text/{x="ITCA"++i".xml";}{print > x;}' $1
