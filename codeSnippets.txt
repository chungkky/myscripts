codeSnippets.txt

------------------
XPATH

//title[@type=“edName”]		# returns all title element nodes with attribute type=“edName”

//title      				# returns all title element nodes



------------------
PANDOC

pandoc -s shellLesson.md -o shellLesson.html
# produce a standalone file, input is markdown, "-o" output is html file

pandoc -f text -t docx *.txt  
# input format is text, output format is docx, source files is *.txt


------------------
GIT

git log --pretty=“%h - %s” --author=<VALUE> --since="2017-02-22"
   # %h is abbreviated hash, %s is subject

------------------
GREP

# counting things

wc <filename>   # returns line, word, charac count for a given file
wc -l <filename>  # returns line count for given file

#
# for each occurance of "@h", returns line number and 
# line containing "@h" in a given file

grep -n "@h" <filename>

#
# outputs to screen the number of "@h" documents within 
# larger file <SalisburyRecords_01.txt>

grep -n "@h" SalisburyRecords_01.txt | wc -l 

#
# outputs into new file <numOfDocs02.txt> the number of 
# "@h" documents within large file <SalisburyRecords_01.txt>\

grep -n "@h" SalisburyRecords_01.txt | wc -l > numOfDocs02.txt

------------------
SED

# steps to separate components of @h-code line


486  ls
  487  cd Temp/
  488  ls
  489  sed 's/\/,/' docsStaff_atHcodes02.txt docsStaff_atHcodes03.txt # wrong, need escape code
  490  sed 's/\\/,/' docsStaff_atHcodes02.txt docsStaff_atHcodes03.txt 
  491  sed 's/\\/,/' docsStaff_atHcodes02.txt > docsStaff_atHcodes03.txt 
  492  ls
  493  cat docsStaff_atHcodes02.txt 
  494  cat docsStaff_atHcodes03.txt 
  495  sed 's/\\!//' docsStaff_atHcodes03.txt > docsStaff_atHcodes04.txt 
  496  cat docsStaff_atHcodes04.txt 
  497  sed 's/!/,/g' docsStaff_atHcodes04.txt > docsStaff_atHcodes05.txt 
  498  cat docsStaff_atHcodes05.txt 


  # steps to remove pagination numbers from index text
  470  cd SampleIndexes/
  471  ls
  472  sed -E -i.tmp 's/[0-9]+-[0-9]+, //g' CheshireIndex.txt
  473  sed -E -i.tmp 's/[0-9]+-[0-9]+//g' CheshireIndex.txt
  474  sed -E -i.tmp 's/[0-9]+, ' CheshireIndex.txt
  475  sed -E -i.tmp 's/[0-9]+, //g' CheshireIndex.txt
  476  sed -E -i.tmp 's/[0-9]+//g' CheshireIndex.txt
  477  sed -E -i.tmp 's/INDEX//g' CheshireIndex.txt
  478  exit

-------------------------------
# finding first word of Doc Code files
/^[A-Z]{3} /gm      # g for global, m for multiple lines but not sure if for processing


---------------------------
# put delimiter ";" at end of 4-letter and then the 3-letter doc code at start of a line; two steps
sed -E 's/^.... /&; /g' input.txt > output01.txt     # replace 4-characters and space with 4-characters and space and ";"
sed -E 's/^[A-Z]{3} /&; /g' output01.txt > output02.txt   # replace 3-letter and space with 3-letter and space and ";"
sed -E 's/ ;/;/g' output02.txt > output03.txt   # replace space+";" with ";"

-------------------------
# remove empty lines using sed
sed -E '/^$/d' input.txt > output.txt


-------------
# Append extension to file name
# From Script Ahoy

for files in *; do mv ${files} ${files}.doc; done


-------------
# Split a large file based on a delimiter line. Resulting files
# have no file extension. So you need to rename resulting files 
# using above script from Script Ahoy to add the file extension.

# From: # <http://www.lostsaloon.com/technology/how-to-split-a-file-into-multiple-files-in-linux/>
# and manual page for "csplit"

csplit -k -n 3 -f output salisDelim.txt /^@@@@@/ {500}

# the "-n 3" says to use 3-digits for the file out put
# the "-f output" makes the prefix of resulting files "output"
# together this means resulting files have name pattern:
# "output000", "output001", "output002", etc.


