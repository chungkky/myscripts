# sed script to remove page references from index text file <oldfile.txt> 
# step 1: substitute "125-6, " with ""
-E -i.tmp 's/[0-9]+-[0-9]+, //g'

# step 2: substitute "125-6" with ""
-E -i.tmp 's/[0-9]+-[0-9]+//g'

# step 3: substitute "125, " with ""
-E -i.tmp 's/[0-9]+, //g'

# step 4: substitute "125" with ""
-E -i.tmp 's/[0-9]+//g'