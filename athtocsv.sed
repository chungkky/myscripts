# This sed script take a file of @H lines
# and converts the data into csv fomrat
#   linenum:@h\STF!1528-9!SSU2\! should become
#   linenum,STF,1528-9,SSU2
#
# to run this script, at command line, enter the line
#   sed -f athtocsv.sed input.txt > output.txt
#
# =====================================================

# change colons to commas
s/:/,/

# delete "@h\" string
s/@h\\//

# delete end "\!" string
s/\\!//

# delete remaining back slash "\"
s/\\//

# replace all exclamation marks "!" with commas ","
s/!/,/g
