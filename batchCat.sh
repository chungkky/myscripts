#!/bin/bash
# Script for batch concatenating RLo files
# 
# variable "a" is a counter and used to name new output files
a=1
for i in ITPB_*.xml; do  # enter source file here
#	echo $i, $a
	new="ITParliamentBook_"$(printf "%02d.xml" "$a")
#	echo $new
	cat head_ITPB.xml "$i" tail_ITPB.xml > "$new"  # enter head filename and tail filename
	let a+=1
done