#!/usr/bin/env python

# File name: renameAtoB.py
# 
# to run this file, enter: $ python renameAtoB-test.py oldfilename newfilename
#
# import sys libary which allow you to use arguments with python CLI
import sys
# sys.argv[0] is the name of the script, sys.argv[1] is the first argument, etc.
# print (sys.argv[0], sys.argv[1], sys.argv[2])

# Read sys.argv[1] (which is the oldfilename or A), into a list, striping away the \n
# at the end of the lines
#
with open(sys.argv[1]) as f:
    RecName = [line.rstrip() for line in f]
#    print(RecName)
#
# Read sys.argv[2] (which is the newfilename or B), into a list, striping away the \n
# at the end of the lines

with open(sys.argv[2]) as g:
    NewRecName = [line.rstrip() for line in g]
#    print(NewRecName)

# import the os library which has the rename function
import os
#
for i in range(len(RecName)):  # the "i" is used for indexing the lists
#	print(RecName[i], NewRecName[i])  # this is to test that the strings are correct
	os.rename(RecName[i], NewRecName[i])
#
# End
