#!/bin/bash
# script to read line of filename from a file and
# create an empty file of given filename
#
# run script:  $ batchCreateFiles-01.sh sourcefile.txt
#
while read filename; do
  touch $filename
done
#
#
# one line version of this script:
# $ while read filename; do touch $filename; done < sourcefile.txt
# 
# OR, to make sure the last line is always read - whether newline-terminated or not:
# $ while read filename || [[ -n $filename ]]; do touch $filename; done < sourcefile.txt
# 